using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vida : MonoBehaviour
{   
    public int vida;
    private int vidaMax;
    private Vector2 checkpoint;

    // Start is called before the first frame update
    void Start()
    {
        checkpoint = transform.position;
        vidaMax = vida;

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        vida -= collision.gameObject.GetComponent<Inimigos>().Dano;
        if(vida <= 0)
        {
            //Destroy(gameObject);
            Respawn();
        }

    }
    
    void Respawn()
    {
        transform.position = checkpoint;
        vida = vidaMax;
    }
}
