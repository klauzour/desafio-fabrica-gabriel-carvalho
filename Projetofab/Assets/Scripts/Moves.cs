using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moves : MonoBehaviour
{   
    public float velocidade;
    public float velPulo;
    private Rigidbody2D rb;
    public bool podePular;
    public float inputDirection;
    public bool isDirectionRight;
    public Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Flip();
        GetInput();
        animController();

        if(Input.GetKeyDown(KeyCode.Space) && podePular == true)
        {
            podePular = false;
            rb.velocity = new Vector2(rb.velocity.x, velPulo);
        }

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("chao"))
        {
            podePular = true;
        }
    }
    private void FixedUpdate()
    {
        MoveLogic();
    }

    void GetInput()
    {
        inputDirection = Input.GetAxis("Horizontal");
    }

    void MoveLogic()
    {
        rb.velocity = new Vector2(inputDirection * velocidade, rb.velocity.y);
    }

    void Flip()
    {
        if(isDirectionRight && inputDirection > 0)
        {
            FlipLogic();
        }
        if(!isDirectionRight && inputDirection < 0)
        {
            FlipLogic();
        }
    }

    void FlipLogic()
    {
        isDirectionRight = !isDirectionRight;
        transform.Rotate(0f, 180.0f, 0f);
    }

    void animController()
    {
        anim.SetFloat ("Horizontal", rb.velocity.x);
        anim.SetFloat ("Vertical", rb.velocity.y);
        anim.SetBool ("PodePular", podePular);
    }

}
